import React from 'react';

import ContentBlock from '../components/ContentBlock';

const JET = window.JET;

class NewsDemo extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            basket:[]
        }
    }

    static path = '/news'

    componentDidMount(){
        this.subscription = window.JET.News.create()
            .newsExpression("A")
            .onAppend(this.onAppend.bind(this))
            .onInsert(onInsert)
            .onDelete(onDelete)
            .start();


        function onInsert(command) {
            // console.log(command)
        }

        function onDelete(command) {
            // console.log(command)
        }
    }

    onAppend(command) {
        const basket = this.state.basket;
        basket.push(command);
        this.setState({
            basket: basket
        });
    }

    componentWillUnmount() {
        this.subscription && this.subscription.stop();
    }

    render(){
        return (
            <ContentBlock title={'News Demo'}>
                {this.state.basket.map((item,i) => {
                    return <div key={i}>{item}</div>
                })}
            </ContentBlock>
        )
    }
}

export default NewsDemo;