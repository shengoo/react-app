import React from 'react';
import {connect} from 'react-redux';

import Actions from '../actions'
import IndexRow from '../components/IndexRow';
import ContentBlock from '../components/ContentBlock';

const rics = [
    'JPY=',
        '.SZSC1',
        '.SZS100PI', '.HSI', '.HSCAHAI', '.HSCAHHI', '.HSCAHI',
        '.HSCAHPI',
        '.HSLI',
        '.HSMI', '.HSLMI', '.HSSI', '.HSCI'
    ],
    fields = [
        'X_RIC_NAME', 'CF_NAME', 'DSPLY_NMLL', 'CF_LAST', 'CF_NETCHNG',
        'PCTCHNG', 'ACVOL_1', 'TURNOVER', 'PERATIO', 'TRDTIM_1', 'TRADE_DATE',
        'LST_PRCTCK',
    ];



class IndexGrids extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const JET = window.JET;
        console.log(JET);
        this.subscription = JET.Quotes.create()
            .pauseOnDeactivate(true)
            .rics(rics)
            .rawFields(fields)
            .onUpdate((sub, ric, data) => {
                this.props.dispatch(Actions.updateRow({
                    sub, ric, data
                }));
            })
            .onError((error) => {
                console.log(error)
            })
            .start();
    }

    componentWillUnmount() {
        this.subscription.stop();
    }

    render() {
        return (
            <ContentBlock title={'Indexes'}>
                <table className="basic-table links-alt-color">
                    <thead>
                    <tr>
                        <td>
                            X_RIC_NAME
                        </td>
                        <td>
                            CF_NAME
                        </td>
                        <td>
                            DSPLY_NMLL
                        </td>
                        <td>
                            CF_LAST
                        </td>
                        <td>
                            CF_NETCHNG
                        </td>
                        <td>
                            PCTCHNG
                        </td>
                        <td>
                            ACVOL_1
                        </td>
                        <td>
                            TURNOVER
                        </td>
                        <td>
                            PERATIO
                        </td>
                        <td>
                            TRDTIM_1
                        </td>
                        <td>
                            TRADE_DATE
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.data &&
                        Object.keys(this.props.data).map(item => (
                            <IndexRow key={item} ric={item} {...this.props.data[item]}/>
                        ))
                    }
                    </tbody>
                </table>
            </ContentBlock>
        )
    }

    static path = '/indexgrids';
}

export default connect(state => ({data: state.index}))(IndexGrids);