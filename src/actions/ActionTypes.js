const ActionTypes = {
    INCREMENT:'INCREMENT',
    DECREMENT:'DECREMENT',
    INDEX_GRID_ROW_UPDATE:'INDEX_GRID_ROW_UPDATE',
};

export default ActionTypes;