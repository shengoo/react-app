import * as UserActions from './user'
import indexGridActions from './indexGridActions'

export default Object.assign({},
    UserActions,
    indexGridActions
);