import ActionTypes from './ActionTypes'

const indexGridActions = {
    updateRow(row){
        return {
            type: ActionTypes.INDEX_GRID_ROW_UPDATE,
            row
        }
    }
}

export default indexGridActions