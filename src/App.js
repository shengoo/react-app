import React, {Component} from 'react';
import {connect} from 'react-redux'
import {BrowserRouter, HashRouter, Route, NavLink} from 'react-router-dom';

import Actions from './actions'

import LoginSatus from './components/LoginStatus'
import Navigation from './components/Navigation';

import IndexGrids from './containers/IndexGrids';
import NewsDemo from './containers/NewsDemo';
import i18next from 'i18next'


const Home = () => (
    <div>Home</div>
)
const About = () => (
    <div>About</div>
)

const Login = () => (
    <div>Login</div>
)

const styles = {
    app: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    main: {
        flex: 1,
        overflow: 'auto'
    }
}

class App extends Component {
    render() {
        console.log(i18next.t('key'))
        return (
            <HashRouter>
                <div style={styles.app}>
                    <div className="navbar">
                        <Navigation/>
                    </div>
                    <div style={styles.main}>
                        <Route exact={true} path="/" component={Home}/>
                        <Route path="/about" component={About}/>
                        <Route path="/login" component={Login}/>
                        <Route path={IndexGrids.path} component={IndexGrids}/>
                        <Route path={NewsDemo.path} component={NewsDemo}/>
                    </div>
                </div>
            </HashRouter>
        );
    }
}

export default App;
