/**
 * Created by UC206612 on 5/3/2017.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';

class ContentBlock extends Component {

  constructor(props){
    super(props);
  }

    render() {
        return (
            <div className="content-block">
                <header className="level1">
                    <h6>{this.props.title}</h6>
                </header>
                <div className="content loading-container">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

ContentBlock.propTypes = {
  title: PropTypes.string.isRequired
};

ContentBlock.defaultProps = {
  title: 'no title'
};

export default ContentBlock;
