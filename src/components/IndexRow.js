import React from 'react';
import i18next from 'i18next'

import InstrumentMovementSpan from './InstrumentMovementSpan';

class IndexRow extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.CF_LAST && this.props.CF_LAST){
            const movement = nextProps.CF_LAST.raw > this.props.CF_LAST.raw ? 'up' :
                nextProps.CF_LAST.raw < this.props.CF_LAST.raw ? 'down':
            '';
            movement && this.setState({
                clMv: movement
            })
            // console.log(movement)
        }
    }
    render(){
        // console.log(this.props)
        const lastClass = this.props.PCTCHNG && this.props.PCTCHNG.raw > 0 ? 'pos' : 'neg';
        const initArrowClass = this.props.PCTCHNG && this.props.PCTCHNG.raw > 0 ? 'icon-arrow-up' : 'icon-arrow-down';
        // const animateClass =
        // console.log(this.state.clMv)
        return (
            <tr>
                <td>
                    {this.props.ric}
                </td>
                <td>
                    {this.props.CF_NAME && this.props.CF_NAME.formatted}
                </td>
                <td>
                    {i18next.language==='en' ? this.props.CF_NAME && this.props.CF_NAME.formatted :
                        this.props.DSPLY_NMLL && this.props.DSPLY_NMLL.formatted}
                </td>
                {this.props.CF_LAST && <InstrumentMovementSpan color={lastClass} value={this.props.CF_LAST.raw}/>}
                <td className={lastClass + ' cell ' + this.state.clMv}>
                    <span>{this.props.CF_NETCHNG && this.props.CF_NETCHNG.formatted}</span>
                </td>
                <td className={lastClass + ' cell ' + this.state.clMv}>
                    <span>{this.props.PCTCHNG && this.props.PCTCHNG.formatted}</span>
                </td>
                <td>
                    {this.props.ACVOL_1 && this.props.ACVOL_1.formatted}
                </td>
                <td>
                    {this.props.TURNOVER && this.props.TURNOVER.formatted}
                </td>
                <td>
                    {this.props.PERATIO && this.props.PERATIO.formatted}
                </td>
                <td>
                    {this.props.TRDTIM_1 && this.props.TRDTIM_1.formatted}
                </td>
                <td>
                    {this.props.TRADE_DATE && this.props.TRADE_DATE.formatted}
                </td>
            </tr>
        )
    }
}

export default IndexRow;