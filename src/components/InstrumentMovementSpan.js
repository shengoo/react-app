import React from 'react';
import ReactDOM from 'react-dom';

class InstrumentMovementSpan extends React.Component{

    constructor(props){
        super(props);
        this.state = {};
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.value && this.props.value){
            // this.setState({
            //     clMv: ''
            // })
            var $this = ReactDOM.findDOMNode(this);
            $this.classList.remove('up');
            $this.classList.remove('down');
            const movement = nextProps.value > this.props.value ? 'up' :
                nextProps.value < this.props.value ? 'down':
                    '';
            movement && this.setState({
                clMv: movement
            })
            this.forceUpdate();
            // console.log(movement)
        }
    }

    componentDidUpdate(){
    }

    render(){
        // console.log('render')
        const classname = (this.state.clMv ? this.state.clMv : '')  + ' ' + this.props.color;
        return(
            <td className={classname}>{this.props.value}</td>
        )
    }
}

export default InstrumentMovementSpan;