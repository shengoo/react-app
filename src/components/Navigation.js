import React from 'react';
import {BrowserRouter, HashRouter, Route, NavLink} from 'react-router-dom';
import IndexGrids from '../containers/IndexGrids';
import NewsDemo from '../containers/NewsDemo'
import i18next from 'i18next';

const navigation = () => (
    <div className="button-split">
        <NavLink exact={true} to="/">
            <button>home</button>
        </NavLink>
        <NavLink to={IndexGrids.path}>
            <button>Index Grid</button>
        </NavLink>
        <NavLink to={NewsDemo.path}>
            <button>News Demo</button>
        </NavLink>
        <NavLink to="/list">
            <button>ListDemo</button>
        </NavLink>
        <NavLink to="/jqc">
            <button>JetQuotesChainDemo</button>
        </NavLink>
        <NavLink to="/jet/apps">
            <button>JET Demos</button>
        </NavLink>
        <button>{i18next.t('key')}</button>
        <div style={{float:'right'}}>
            <button onClick={() => {
                i18next.changeLanguage('en')
            }}>
                english
            </button>
            <button onClick={() => {
                i18next.changeLanguage('zh')
            }}>
                中文
            </button>
        </div>
    </div>
);

export default navigation;