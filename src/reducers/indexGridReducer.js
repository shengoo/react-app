import ActionTypes from '../actions/ActionTypes'

export default (state = {},action) => {
    switch (action.type){
        case ActionTypes.INDEX_GRID_ROW_UPDATE:
            return {
                ...state,
                [action.row.ric]: {
                    ...(state[action.row.ric]||{}),
                    ...action.row.data},
            };
        default:
            return state;
    }
}