import { combineReducers } from 'redux';

import user from './user';
import index from './indexGridReducer'

const rootReducer = combineReducers({
    user,
    index
});

export default rootReducer;