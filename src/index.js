import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'

import './styles/app.css';
import './styles/price-change.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './store'

import i18next from 'i18next'
import 'jet-api'
import 'jet-plugin-settings/src/jet-plugin-settings'
import 'jet-plugin-apphits/src/jet-plugin-apphits'
import 'jet-plugin-quotes/src/jet-plugin-quotes'
// import 'jet-plugin-news/dist/JET.News-with-deps'
// import './jet-plugin-news/src/jet-plugin-news'
// import 'news-jet-plugin/src/News'
console.log(i18next)
document.write(`<script src="jet-plugin-news/src/jet-plugin-news.js"></script>`)
const JET = window.JET;

function startApp() {
    window.top.document.title = 'React App';
    JET.appHit('ng-app', 'app', 'start');
    ReactDOM.render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById('root'));
    registerServiceWorker();
    JET.Settings.read(function (value) {
        document.body.className += tickColorClass[value];
    }, {
        providerName: 'Configuration',
        settingName: 'RDE_USER_CURRENT_TICK_COLOR'
    });
}
function refreshApp() {
    ReactDOM.render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById('root'));
}

const tickColorClass = {
    'European': 'emea',
    'American': 'amers',
    'Asian1': 'apac1',
    'Asian2': 'apac2'
};
const jetPromise = new Promise((resolve,reject) => {
    JET.onLoad(() => {
        resolve();
    });
});

JET.init({
    ID: 'React-app'
});

// const i18next = window.i18next;
const i18nextPromise = new Promise((resolve,reject) => {
    i18next.init({
        lng: 'en',
        debug: true,
        resources: {
            en: {
                translation: {
                    "key": "hello world"
                }
            },
            zh: {
                translation: {
                    "key": "你好"
                }
            }
        }
    }, function(err, t) {
        // init set content
        updateContent();
        resolve();
    });
});

function updateContent() {
    console.log(i18next.t('key'));
}

function changeLng(lng) {
    i18next.changeLanguage(lng);
}

i18next.on('languageChanged', () => {
    updateContent();
    refreshApp();
});

Promise.all([jetPromise,i18nextPromise]).then(startApp).catch(reason=>{console.log(reason)});